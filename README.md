# WS Assignment - Demo

## Run the application locally

**Prerequisites:** 
* Installed PHP 7.0 or later.
* Installed Node and Node package manager (npm)

**Run the application:**
* Run script: `./run.sh`

## Run the application via Nginx / Apache
* Setup your webroot to `public` directory

## Rebuild the Assets
* Run `npm install` and install all required packages.
* Run `gulp` or `gulp release` to rebuild all assets (CSS and Javascripts).
* Run `gulp watch` to automatically rebuild after the changes detected.
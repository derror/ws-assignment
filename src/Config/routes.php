<?php

return [
    "/" => "MainController:defaultAction",
    "/API/record/list" => "APIController:listAction",
    "/API/record/add" => "APIController:addAction",
    "/API/record/delete" => "APIController:deleteAction",
    "/API/record/type/list" => "APIController:typeListAction",
];
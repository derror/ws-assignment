<?php

/**
 * The main Config
 */

return [
    /* Some basic settings */
    "application_name" => "Hello WebSupport!",

    /* WebSupport REST API credentials */
    "ws_client_username" => "ws-php-assignment",
    "ws_client_secret" => "EmPX8fnp",

    /* WebSupport REST API values */
    "ws_default_domain" => "php-assignment.eu",
    "ws_default_domain_id" => 48790
];
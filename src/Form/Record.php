<?php

namespace Form;

class Record
{

    /**
     * Do simple form validation
     *
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function isValid($data)
    {
        if (empty($data) || empty($data['type'])) {
            throw new \Exception('Form validation error. Empty form.');
        }

        if (!in_array($data['type'], self::getRecordTypes())) {
            throw new \Exception('Unknown record type: ' . $data['type']);
        }
        return true;
    }

    /**
     * Get all available record types
     *
     * @return array
     */
    public static function getRecordTypes()
    {
        return [
            'A',
            'AAAA',
            'ANAME',
            'CNAME',
            'NS',
            'MX',
            'TXT',
            'SRV'
        ];
    }

}
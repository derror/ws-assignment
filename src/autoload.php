<?php

/**
 * Set-up src directory
 */
define('BASE_PATH', realpath(dirname(__FILE__)));

function class_autoloader($class)
{
    $basePath  = BASE_PATH;
    $classPath = str_replace('\\', '/', $class);
    include("${basePath}/${classPath}.php");
}

spl_autoload_register('class_autoloader');
<?php

namespace Core;

use Core\Components\Request;
use Core\Components\Response;
use Core\Components\ResponseInterface;
use Helper\ApiClient;

class Application
{

    private $config = [];
    private $router = [];
    private $controllerPath;
    static $instance;
    const CONTROLLER_DIR = "Controller";


    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @param array $config
     * @throws \Exception
     */
    public function setup($config)
    {
        $this->config = $config;

        /**
         * Router set-up
         */
        $this->router = new Router(
            include(BASE_PATH . "/Config/routes.php")
        );

        /**
         * API Client set-up
         */
        $this->apiClient = new ApiClient(
            [
                "username" => $this->getConfigVal("ws_client_username"),
                "secret" => $this->getConfigVal("ws_client_secret")
            ],
            [
                'domain_name' => $this->getConfigVal("ws_default_domain")
            ]);

        $this->controllerPath = BASE_PATH . '/' . self::CONTROLLER_DIR;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function getConfigVal($name)
    {
        if (empty($this->config[$name])) {
            throw new \Exception("Config value ${name} not found!");
        }
        return $this->config[$name];
    }

    public function receiveRequest(Request $request)
    {
        $route = $this->router->resolve($request);
        if (!$route) {
            return new Response("Page not found!", 404);
        }

        /**
         * Resolve the Controller routine
         */
        $controllerClass  = $route['class'];
        $controllerAction = $route['method'];
        $controllerPath   = self::CONTROLLER_DIR . '\\' . $controllerClass;

        try {
            $controller = new $controllerPath();
            $response   = $controller->$controllerAction($request);

        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }

        if (!$response instanceof ResponseInterface) {
            throw new \Exception("Return value of the controllerAction must be instance of Response object!");
        }

        return $response;
    }

    /**
     * @return ApiClient
     */
    public function getApiClient(): ApiClient
    {
        return $this->apiClient;
    }

    public static function instance()
    {
        return self::$instance;
    }
}
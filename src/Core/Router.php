<?php

namespace Core;

use Core\Components\Request;

class Router
{
    private $routes;

    /**
     * Router constructor.
     * @param $routes
     */
    public function __construct($routes)
    {
        $this->routes = array_change_key_case($routes, CASE_LOWER);
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function resolve(Request $request)
    {
        $uri = strtolower($request->uri);

        if (empty($this->routes[$uri])) {
            return [];
        }

        $route = $this->routes[$uri];
        preg_match('/^([a-z]+)\:([a-z]+)$/is', $route, $foundController);

        if (empty($foundController[1]) || empty($foundController[2])) {
            throw new \Exception('Router error. Something went wrong!');
        }

        /**
         * Normalization
         */
        $controllerName = str_replace('controller', '', strtolower($foundController[1]));
        $actionName     = str_replace('action', '', strtolower($foundController[2]));

        return [
            'class' => ucfirst($controllerName . "Controller"),
            'method' => ucfirst($actionName . "Action")
        ];

    }
}
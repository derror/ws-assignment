<?php

namespace Core\Components;

class Request
{
    public $parameters = [];
    public $uri;

    public static function build()
    {
        $request             = new self();
        $request->parameters = array_merge($_GET, $_POST);
        $request->uri        = strtok($_SERVER['REQUEST_URI'], '?');

        return $request;
    }

    /**
     * Returns POST & GET parameters
     *
     * @TODO: Sanitize params
     * @return array
     */
    public function getAllParameters()
    {
        return $this->parameters;
    }

    /**
     * Returns value from POST or GET parameters
     *
     * @TODO: Sanitize params
     * @param $name
     * @return mixed
     */
    public function getParameter($name)
    {
        if (isset($this->parameters[$name])) {
            return $this->parameters[$name];
        }
    }
}
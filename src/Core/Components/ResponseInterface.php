<?php

namespace Core\Components;

/**
 * Interface ResponseInterface
 * @package Core\Components
 */
interface ResponseInterface
{
    /**
     * ResponseInterface constructor.
     * @param $body
     * @param $httpCode
     */
    public function __construct($body, $httpCode);

    public function set($body, $httpCode);

    /**
     * Send all the data to the output
     */
    public function output();

    public function getBody(): string;

    public function setBody($body);

    public function getCode(): int;

    public function setCode($httpCode);
}
<?php

namespace Core\Components;

class JsonResponse implements ResponseInterface
{
    private $body;
    private $code;

    /**
     * JsonResponse constructor.
     * @param array $data
     * @param int $httpCode
     */
    public function __construct($data = [], $httpCode = 200)
    {
        $this->body = $data;
        $this->code = $httpCode;
    }

    public function set($body = [], $httpCode = 200)
    {
        $this->body = $body;
        $this->code = $httpCode;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function output()
    {
        $code = $this->code;
        header("Cache-Control: no-cache, must-revalidate");
        header("Content-Type: application/json");
        header("HTTP/1.0 {$code}");
        echo(json_encode($this->body));
        exit;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
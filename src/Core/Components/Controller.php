<?php

namespace Core\Components;

class Controller
{
    const VIEWS_PATH = BASE_PATH . '/View';

    /**
     * @return \Core\Application
     */
    public function getApplication()
    {
        $instance = &getApplication();
        return $instance;
    }

    /**
     * Render given template
     *
     * @param $template
     * @param array $data
     * @return Response
     * @throws \Exception
     */
    public function render($template, $data = [])
    {
        $templatePath = self::VIEWS_PATH . '/' . $template;
        if(!is_readable($templatePath)) {
            throw new \Exception("Template ${templatePath} doesn't exist!");
        }
        return new Response(file_get_contents(self::VIEWS_PATH . '/' . $template));
    }
}
$(function () {
    const app = {
        wsDomainName: "php-assignment.eu", // this should be a dynamic value
        leftMenuComponent: undefined,
        $alertsWrapper: $('.alerts-wrapper'),
        typesList: {},
        init: function () {
            $.when(app.getRecordTypes().done(function (data, status) {
                app.typesList = data;
                leftMenuComponent.init();
                listComponent.init();
                addModalComponent.init();
            }));

            $("#add-new-record").on('click', function () {
                addModalComponent.open();
            });

            $(window).on('hashchange', function () {
                const type = window.location.hash.replace('#', '');
                leftMenuComponent.selectedType = type;
                leftMenuComponent.reload();
                listComponent.reload();
                app.updateHeadingText();
            });

            app.updateHeadingText();
            leftMenuComponent.reload();

        },
        getRecordTypes: function () {
            app.loading(true);
            return $.ajax('/API/record/type/list',
                {
                    dataType: 'json', // type of response data
                    timeout: 5000,     // timeout milliseconds
                    success: function (data, status, xhr) {
                        app.typesList = data;
                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        console.log("Error: " + errorMessage);
                    }
                }).done(function () {
                app.loading(false);
            });
        },
        alert: function (message, type) {
            const alert = $('<div class="alert ' + type + '" role="alert">' + message + '</div>').fadeOut(3000);
            app.$alertsWrapper.append(alert);
        },
        updateHeadingText: function () {
            let type = window.location.hash.replace('#', '');
            type = type ? type : leftMenuComponent.selectedType;
            $(".heading-line .type-name").text('"' + type + '"');
        },
        loading: function (enabled) {
            if (enabled) {
                $("#loader").show();
            } else {
                $("#loader").hide();
            }
        }
    };

    const addModalComponent = {
        $context: $('#addModal'),
        leftMenuComponent: undefined,

        init: function () {
            addModalComponent.reloadType();
            addModalComponent.bindActions();

        },
        bindActions: function () {
            /* Bind Submit button*/
            addModalComponent.$context.find('.btn.submit').on('click', function () {
                addModalComponent.submit();
            });
        },
        close: function () {
            addModalComponent.$context.modal('toggle');
        },
        open: function () {
            const source = document.getElementById("add-modal").innerHTML;
            const template = Handlebars.compile(source);

            const html = template({
                domain: app.wsDomainName,
                type: leftMenuComponent.selectedType
            });
            /* Reload the Sidebar menu*/
            $('#addModal').html(html);
            addModalComponent.$context = $('#addModal');
            addModalComponent.bindActions();

            addModalComponent.$context.modal('toggle');
        },
        reloadType: function () {
            const $selectbox = this.$context.find('#record-type');
            const types = app.typesList;

            $.each(types, function (key, value) {
                $selectbox.append("<option value=" + value + ">" + value + "</option>");
            });
        },
        submit: function () {
            let formData = removeEmpty({
                type: addModalComponent.$context.find('input.record-type').val(),
                name: addModalComponent.$context.find('input.record-name').attr('disabled') ? '' : addModalComponent.$context.find('input.record-name').val(),
                content: addModalComponent.$context.find('input.record-value').val(),
                ttl: addModalComponent.$context.find('input.record-ttl').val(),
                prio: addModalComponent.$context.find('input.record-priority').val(),
                port: addModalComponent.$context.find('input.record-port').val(),
                weight: addModalComponent.$context.find('input.record-weight').val()
            });

            app.loading(true);
            addModalComponent.close();

            $.ajax('/API/record/add',
                {
                    dataType: 'json',
                    method: 'post',
                    timeout: 5000,
                    data: formData,
                    success: function (data, status, xhr) {
                        if (data.status == 'success') {
                            app.alert('Record was added successfully.', 'alert-primary');
                            listComponent.reload();
                        } else {
                            app.alert("Record wasn't added.", 'alert-danger');
                            app.alert(data.errors.content[0], 'alert-warning')
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        app.alert('Error: ' + errorMessage, 'alert-dander');

                    }
                }).done(function () {
                app.loading(false);
            });
        }
    };

    const listComponent = {
        leftMenuComponent: undefined,
        modalConfirm: function (callback) {
            $("#rm-btn-yes").on("click", function () {
                callback(true);
                $("#removeSubmitModal").modal('hide');
            });

            $("#rm-btn-no").on("click", function () {
                callback(false);
                $("#removeSubmitModal").modal('hide');
            })
        },
        init: function () {
            listComponent.reload();
        },
        removeRecord: function (recordId) {
            app.loading(true);
            $.ajax('/API/record/delete',
                {
                    dataType: 'json',
                    timeout: 5000,
                    method: 'post',
                    data: {
                        id: recordId
                    },
                    success: function (data, status, xhr) {
                        app.alert('Record was successfully removed.', 'alert-primary');
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        console.log("Error: " + errorMessage);
                    }
                }).done(function () {
                app.loading(false);
            });
        },
        reload: function () {
            const source = document.getElementById("list-template").innerHTML;
            const template = Handlebars.compile(source);
            let type = window.location.hash.replace('#', '');
            type = type ? type : leftMenuComponent.selectedType;

            let url = '/API/record/list';
            app.loading(true);
            $.ajax(url,
                {
                    dataType: 'json',
                    timeout: 5000,
                    method: 'get',
                    data: {
                        filter: {
                            type: type
                        }
                    },
                    success: function (data, status, xhr) {
                        let html = template({
                            list: data.items,
                            type: type,
                            domain: app.wsDomainName
                        });

                        listComponent.modalConfirm(function (confirm) {
                            const $submitModal = $("#removeSubmitModal");
                            if (confirm) {
                                listComponent.removeRecord($submitModal.attr('data-id'));
                                listComponent.reload();
                            }
                            $submitModal.modal('hide');
                        });

                        /* Reload the Sidebar menu*/
                        $('#record-list').html(html).find('button.remove').on('click', function () {
                            $('#removeSubmitModal').attr('data-id', $(this).attr('data-id')).modal('toggle');
                        });
                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        console.log("Error: " + errorMessage);
                    }
                }).done(function () {
                app.loading(false);
            });
        }
    };

    const leftMenuComponent = {
        selectedType: 'A',
        reload: function () {
            const source = document.getElementById("left-menu-template").innerHTML;
            const template = Handlebars.compile(source);

            const html = template({
                recordTypes: app.typesList,
                selected: leftMenuComponent.selectedType
            });
            /* Reload the Sidebar menu*/
            $('#menu-sidebar').html(html);
        },

        init: function () {
            leftMenuComponent.reload();
        }
    };


    app.init();
});

//app.leftMenuComponent = leftMenuComponent;

Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

Handlebars.registerHelper('ifContain', function (arg1, arg2, options) {
    const vars = arg2.split(',');
    return (vars.indexOf(arg1) >= 0) ? options.fn(this) : options.inverse(this);
});

const removeEmpty = (obj) =>
    Object.keys(obj)
        .filter(f => obj[f] != null)
        .reduce((r, i) =>
                typeof obj[i] === 'object' ?
                    {...r, [i]: removeEmpty(obj[i])} :  // recurse.
                    {...r, [i]: obj[i]},
            {});
<?php

namespace Controller;

use Core\Components\Controller;
use Core\Components\Request;
use Core\Components\Response;

class MainController extends Controller
{

    public function defaultAction(Request $request)
    {
        return $this->render("layout.html");
    }
}
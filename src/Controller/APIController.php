<?php

namespace Controller;

use Core\Components\Controller;
use Core\Components\JsonResponse;
use Core\Components\Request;
use Form\Record;

class APIController extends Controller
{
    /**
     * List of all records
     * Filters:
     *  1. type
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAction(Request $request)
    {
        $apiClient = $this->getApplication()->getApiClient();
        $data      = $apiClient->listRecords();
        $list      = json_decode($data);

        if (json_last_error()) {
            throw new \Exception('Data inconsistency error.');
        }

        $filter = $request->getParameter('filter');

        if ($filter && !empty($filter['type'])) {
            $list = $this->filterListByType($list, $filter['type']);
        }

        return new JsonResponse($list);
    }


    public function addAction(Request $request)
    {
        $data = $request->getAllParameters();

        $result = [];

        if (Record::isValid($data)) {
            $apiClient = $this->getApplication()->getApiClient();
            $result    = $apiClient->addRecord($data);
            $result    = json_decode($result);
        }
        return new JsonResponse($result);
    }

    public function deleteAction(Request $request)
    {
        $recordId = $request->getParameter('id');
        if( !$recordId ) {
            throw new \Exception('Parameter id is missing');
        }

        $apiClient = $this->getApplication()->getApiClient();
        $result    = $apiClient->deleteRecord($recordId);

        return new JsonResponse($result);
    }

    public function typeListAction(Request $request) {
        return new JsonResponse(Record::getRecordTypes());
    }

    /**
     * @param $list
     * @param $type
     * @return mixed
     *
     * @TODO: move to the better place
     * @throws \Exception
     */
    private function filterListByType($list, $type)
    {
        $type               = strtoupper($type);
        $filtered[]['items']        = [];
        $filtered['pager']  = $list->pager;
        $filtered['filter'] = [
            'type' => $type
        ];

        if (!in_array($type, Record::getRecordTypes())) {
            throw new \Exception("Unknown DNS type ${type}");
        }

        foreach ($list->items as $i => $item) {
            if ($item->type == $type) {
                $filtered['items'][] = $item;
            }
        }

        return $filtered;
    }


}
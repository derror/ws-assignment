<?php

namespace Helper;

/**
 * Class ApiClient
 * @package Helper
 */
class ApiClient
{
    private $username;
    private $secret;
    private $options;

    public function __construct($credentials, $options = [])
    {
        $this->username = $credentials['username'];
        $this->secret   = $credentials['secret'];
        $this->options  = $options;
    }

    public function request($url, $postData = [], $method = 'GET')
    {
        $jsonPayload = json_encode($postData);

        /**
         * CURL initialization
         */
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonPayload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Content-Length: ' . strlen($jsonPayload),
                //"Authorization : Basic ${encodedAuth}"
            ));
        } elseif ($method == 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }

        /**
         * Curl: Basic Auth credentials
         */
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->secret);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpCode >= 400) {
            throw new \Exception("Http error. Code: ${httpCode}, Body: ${response}");
        }

        return $response;
    }

    /**
     * List all records
     *
     * @return mixed
     * @throws \Exception
     */
    public function listRecords()
    {
        $domainName = $this->options['domain_name'];
        $URL        = "https://rest.websupport.sk/v1/user/self/zone/${domainName}/record";

        return $this->request($URL);
    }

    /**
     * Add record
     *
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function addRecord($data)
    {
        $domainName = $this->options['domain_name'];
        $URL        = "https://rest.websupport.sk/v1/user/self/zone/$domainName/record";

        return $this->request($URL, $data, 'POST');
    }

    /**
     * Delete record
     *
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function deleteRecord($recordId)
    {
        $domainName = $this->options['domain_name'];
        $URL        = "https://rest.websupport.sk/v1/user/self/zone/$domainName/record/${recordId}";

        return $this->request($URL, null, 'DELETE');
    }
}
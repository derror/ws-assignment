'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const livereload = require('gulp-livereload');

gulp.task('sass', function () {
    return gulp.src('./src/Resource/**/*.scss')
        .pipe(sass())
        .pipe(concat('app.css'))
        .pipe(gulp.dest('./public/assets/dist'));
});


gulp.task('js', function () {
    return gulp.src('./src/Resource/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/assets/dist'))
        .pipe(livereload());
});

gulp.task('html', function () {
    return gulp.src('./src/View/**/*.html')
        .pipe(livereload());
});


gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('./src/Resource/*.scss', ['sass']);
    gulp.watch('./src/Resource/*.js', ['js']);
    gulp.watch('./src/View/*.html', ['html']);
});


gulp.task('default', ['sass', 'js']);
gulp.task('release', ['sass', 'js', 'watch']);

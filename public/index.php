<?php

use Core\Application;
use Core\Components\Request;

/**
 * Perform autoload
 */
include_once(__DIR__ . '/../src/autoload.php');

$application = new Application();
$config      = include_once(__DIR__ . '/../src/Config/main.php');
$application->setup($config);


function &getApplication()
{
    $application = Application::instance();
    return $application;
}


$application = &getApplication();

$request  = Request::build();
$response = $application->receiveRequest($request);

$response->output();
